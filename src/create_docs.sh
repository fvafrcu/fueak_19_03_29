#!/bin/sh

# fake some random docs
head -100000 /dev/urandom > presentation.asc
head -110000 /dev/urandom > presentation_v1.asc
head -130000 /dev/urandom > presentation_v1.asc
head -170000 /dev/urandom > presentation_final.asc
head -130000 /dev/urandom > presentation_final_01.asc
head -170000 /dev/urandom > presentation_final_2017_12_21.asc
head -170000 /dev/urandom > presentation_final_07_09_2017.asc
