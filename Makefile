all: presentation.html presentation_alt.html
presentation.html: presentation.asc
	 asciidoc -b slidy presentation.asc  
presentation_alt.html: presentation.asc
	 sed -e "s/, width=1200"// < presentation.asc > presentation_alt.asc; asciidoc -b slidy presentation_alt.asc  
